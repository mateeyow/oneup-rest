var Sails = require('sails');

before(function (done) {
	Sails.lift({
		log: { level: 'error' }
	}, function (err, sails) {
		if (err) return done(err);
		done(null, sails);
	});
});	

after(function (done) {
	sails.lower(done);
});