var chai    = require('chai');
var request = require('superagent');
var should  = chai.should();

chai.use(require('chai-http'));

var agent = chai.request.agent('http://localhost:9999');

describe('Auth0 Testing', function () {
	it('should return 401 or unauthorized request', function (done) {
		agent.get('/secure')
		.end(function (err, response) {
			response.should.have.status(401);
			done();
		});
	});

	it('should issue a 404 error', function (done) {
		agent.get('/test')
		.end(function (err, response) {
			response.should.have.status(404);
			response.body.error.should.equal('Not Found');
			done();
		});
	});

	describe('after authentication', function () {
		var token;

		before(function (done) {
			request.post('https://mateeyow.au.auth0.com/oauth/ro')
			.send({
				client_id: 'HaXfux2zwvrt5TV224CGsY04XZBZAS7B',
				username: 'matthew@mail.com',
				password: 'test',
				connection: 'mongolab',
				grant_type: 'password',
				scope: 'openid'
			})
			.end(function (err, response) {
				token = response.body.id_token;
				done();
			});
		});

		it('should return an http status of 200', function (done) {
			agent.get('/secure')
			.set('Authorization', `Bearer ${token}`)
			.end(function (err, response) {
				response.should.have.status(200);
				done();
			});
		});
	})
});