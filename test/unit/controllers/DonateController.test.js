var chai    = require('chai');
var request = require('superagent');
var should  = chai.should();

chai.use(require('chai-http'));

var agent = chai.request.agent('http://localhost:9999');

describe('Donate Controller', function () {
	var token;

	before(function (done) {
		request.post('https://mateeyow.au.auth0.com/oauth/ro')
		.send({
			client_id: 'HaXfux2zwvrt5TV224CGsY04XZBZAS7B',
			username: 'matthew@mail.com',
			password: 'test',
			connection: 'mongolab',
			grant_type: 'password',
			scope: 'openid'
		})
		.end(function (err, response) {
			token = response.body.id_token;
			done();
		});
	});

	it('should issue an error', function (done) {
		agent.post('/api/v1/donate')
		.set('Authorization', `Bearer ${token}`)
		.end(function (error, response) {
			response.should.have.status(500);
			done();
		});
	});
});