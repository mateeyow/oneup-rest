var chai    = require('chai');
var request = require('superagent');
var should  = chai.should();

chai.use(require('chai-http'));

var agent = chai.request.agent('http://localhost:9999');

describe('Transaction Controller', function () {
	var token;

	before(function (done) {
		request.post('https://mateeyow.au.auth0.com/oauth/ro')
		.send({
			client_id: 'HaXfux2zwvrt5TV224CGsY04XZBZAS7B',
			username: 'matthew@mail.com',
			password: 'test',
			connection: 'mongolab',
			grant_type: 'password',
			scope: 'openid'
		})
		.end(function (err, response) {
			token = response.body.id_token;
			done();
		});
	});

	it('should list an array of transactions', function (done) {
		agent.get('/api/v1/transactions')
		.set('Authorization', `Bearer ${token}`)
		.end(function (err, response) {
			response.should.have.status(200);
			response.body.should.be.an.instanceof(Array);
			done();
		});
	});
});