# Oneup Api

The API endpoint can be found on this link: [https://oneup-testing.herokuapp.com/](https://oneup-testing.herokuapp.com/)

Test if JWT is functional by going to this link: [https://oneup-testing.herokuapp.com/secure](https://oneup-testing.herokuapp.com/secure). It should issue an **Unauthorized Error**

For testing front-end call clone this repository [https://github.com/mateeyow/oneup-front](https://github.com/mateeyow/oneup-front)