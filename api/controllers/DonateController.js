module.exports = {
	donate: function (req, res) {
		var stripe = require('stripe')(process.env.STRIPE_SECRET);

		var stripeToken = req.body.stripeToken;
		var amount      = 1000;

		stripe.charges.create({
			card: stripeToken,
			currency: 'usd',
			amount: amount
		}, function (err, charge) {
			if (err) {
				res.status(500);
				return res.json({
					status: 500,
					message: 'Invalid request'
				});
			}

			if (charge) {
				var data = {
					transactionId: charge.id,
					amount: charge.amount,
					status: charge.status,
					userId: req.user.sub,
					cardType: charge.source.brand
				};
				
				Transactions
				.create(data)
				.exec(function (error, transaction) {
					if (error) return;

					return res.json({
						status: 200,
						message: 'Successfully donated $10.00'
					});
				});
			}
		});
	}
};