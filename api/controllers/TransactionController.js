module.exports = {
	list: function (req, res) {
		Transactions.find()
		.sort('createdAt DESC')
		.exec(function (error, transactions) {
			if (error) return;

			return res.json(transactions);
		});
	}
};