module.exports = {
	schema: true,
	attributes: {
		transactionId: 'string',
		amount: 'integer',
		status: 'string',
		userId: 'string',
		cardType: 'string'
	}
};
